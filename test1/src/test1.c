#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#define num_branches 10
#define RUNTIME 30

char random_chars[num_branches];

int main(void)
{ 
    //populate chars
    for(int i = 0; i < num_branches; i++){
        random_chars[i] = 1;
    }
    //forever
    uint16_t count;
    clock_t start = clock();   
    clock_t end;
    for (count = 0;;count++){
        //do the branches x10
        if(random_chars[0]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[0];
            b+=random_chars[0];
            c+=random_chars[0];
            d+=random_chars[0];
        }
        if(random_chars[1]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[1];
            b+=random_chars[1];
            c+=random_chars[1];
            d+=random_chars[1];
        }
        if(random_chars[2]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[2];
            b+=random_chars[2];
            c+=random_chars[2];
            d+=random_chars[2];
        }
        if(random_chars[3]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[3];
            b+=random_chars[3];
            c+=random_chars[3];
            d+=random_chars[3];
        }
        if(random_chars[4]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[4];
            b+=random_chars[4];
            c+=random_chars[4];
            d+=random_chars[4];
        }
        if(random_chars[5]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[5];
            b+=random_chars[5];
            c+=random_chars[5];
            d+=random_chars[5];
        }
        if(random_chars[6]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[6];
            b+=random_chars[6];
            c+=random_chars[6];
            d+=random_chars[6];
        }
        if(random_chars[7]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[7];
            b+=random_chars[7];
            c+=random_chars[7];
            d+=random_chars[7];
        }
        if(random_chars[8]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[8];
            b+=random_chars[8];
            c+=random_chars[8];
            d+=random_chars[8];
        }
        if(random_chars[9]){
            char 
                a = random_chars[0]/random_chars[1],
                b = random_chars[2]/random_chars[3],
                c = random_chars[3]/random_chars[4],
                d = random_chars[5]/random_chars[6]
            ;
            a+=random_chars[9];
            b+=random_chars[9];
            c+=random_chars[9];
            d+=random_chars[9];
        }
        if(!count){
            end = clock();
            float seconds = (float)(end - start)/CLOCKS_PER_SEC;
            if( seconds >= RUNTIME )
                break;
        }
    }
    return 0;
}
