#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#define num_branches 20
#define RUNTIME 40

uint8_t random_chars[num_branches];

int main(void)
{ 
    //populate chars
    //srand(time(NULL));
    for(int i = 0; i < num_branches; i++){
        random_chars[i] = (uint8_t)((double)rand() / (RAND_MAX + 1) * (256 - 1) + 1) & 1;
        random_chars[i]++;
        printf("%u: %u\n", i, random_chars[i]);
    }
    //forever
    uint16_t count;
    clock_t start = clock();   
    clock_t end;
    for (count = 0;;count++){
        //do the branches x10
        if(random_chars[0] == 1 ){
            uint8_t 
                a = random_chars[6]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[7]/random_chars[4],
                d = random_chars[9]/random_chars[6]
            ;
            a+=random_chars[0];
            b+=random_chars[0];
            c+=random_chars[0];
            d+=random_chars[0];
        }
        else if(random_chars[count % 1] == 1){
            uint8_t 
                a = random_chars[3]/random_chars[1],
                b = random_chars[4]/random_chars[3],
                c = random_chars[7]/random_chars[4],
                d = random_chars[0]/random_chars[6]
            ;
            a+=random_chars[1];
            b+=random_chars[1];
            c+=random_chars[1];
            d+=random_chars[1];
        }
        else if(random_chars[count % 2] == 1){
           uint8_t
                a = random_chars[4]/random_chars[1],
                b = random_chars[0]/random_chars[3],
                c = random_chars[2]/random_chars[4],
                d = random_chars[7]/random_chars[6]
            ;
            a+=random_chars[2];
            b+=random_chars[2];
            c+=random_chars[2];
            d+=random_chars[2];
        }
        else if(random_chars[count % 3] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[3]/random_chars[3],
                c = random_chars[1]/random_chars[4],
                d = random_chars[2]/random_chars[6]
            ;
            a+=random_chars[3];
            b+=random_chars[3];
            c+=random_chars[3];
            d+=random_chars[3];
        }
        else if(random_chars[count % 4] == 1){
           uint8_t
                a = random_chars[2]/random_chars[1],
                b = random_chars[1]/random_chars[3],
                c = random_chars[0]/random_chars[4],
                d = random_chars[1]/random_chars[6]
            ;
            a+=random_chars[4];
            b+=random_chars[4];
            c+=random_chars[4];
            d+=random_chars[4];
        }
        else if(random_chars[count % 5] == 1){
           uint8_t
                a = random_chars[7]/random_chars[1],
                b = random_chars[9]/random_chars[4],
                c = random_chars[9]/random_chars[7],
                d = random_chars[3]/random_chars[6]
            ;
            a+=random_chars[5];
            b+=random_chars[5];
            c+=random_chars[5];
            d+=random_chars[5];
        }
        else if(random_chars[count % 6] == 1){
           uint8_t
                a = random_chars[4]/random_chars[1],
                b = random_chars[7]/random_chars[3],
                c = random_chars[2]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[6];
            b+=random_chars[6];
            c+=random_chars[6];
            d+=random_chars[6];
        }
        else if(random_chars[count % 7] == 1){
           uint8_t
                a = random_chars[9]/random_chars[1],
                b = random_chars[7]/random_chars[2],
                c = random_chars[5]/random_chars[7],
                d = random_chars[6]/random_chars[9]
            ;
            a+=random_chars[7];
            b+=random_chars[7];
            c+=random_chars[7];
            d+=random_chars[7];
        }
        else if(random_chars[count % 8] == 1){
           uint8_t
                a = random_chars[8]/random_chars[4],
                b = random_chars[3]/random_chars[3],
                c = random_chars[6]/random_chars[4],
                d = random_chars[4]/random_chars[8]
            ;
            a+=random_chars[8];
            b+=random_chars[8];
            c+=random_chars[8];
            d+=random_chars[8];
        }
        else if(random_chars[count % 9] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[9];
            b+=random_chars[9];
            c+=random_chars[9];
            d+=random_chars[9];
        }
        else if(random_chars[count % 10] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[10];
            b+=random_chars[10];
            c+=random_chars[10];
            d+=random_chars[10];
        }
        else if(random_chars[count % 11] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[11];
            b+=random_chars[11];
            c+=random_chars[11];
            d+=random_chars[11];
        }
        else if(random_chars[count % 12] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[12];
            b+=random_chars[12];
            c+=random_chars[12];
            d+=random_chars[12];
        }
        else if(random_chars[count % 13] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[13];
            b+=random_chars[13];
            c+=random_chars[13];
            d+=random_chars[13];
        }
        else if(random_chars[count % 14] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[14];
            b+=random_chars[14];
            c+=random_chars[14];
            d+=random_chars[14];
        }
        else if(random_chars[count % 15] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[15];
            b+=random_chars[15];
            c+=random_chars[15];
            d+=random_chars[15];
        }
        else if(random_chars[count % 16] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[16];
            b+=random_chars[16];
            c+=random_chars[16];
            d+=random_chars[16];
        }
        else if(random_chars[count % 17] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[17];
            b+=random_chars[17];
            c+=random_chars[17];
            d+=random_chars[17];
        }
        else if(random_chars[count % 18] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[18];
            b+=random_chars[18];
            c+=random_chars[18];
            d+=random_chars[18];
        }
        else if(random_chars[count % 19] == 1){
           uint8_t
                a = random_chars[5]/random_chars[1],
                b = random_chars[5]/random_chars[3],
                c = random_chars[5]/random_chars[4],
                d = random_chars[8]/random_chars[6]
            ;
            a+=random_chars[19];
            b+=random_chars[19];
            c+=random_chars[19];
            d+=random_chars[19];
        }
        if(!count){
            end = clock();
            float seconds = (float)(end - start)/CLOCKS_PER_SEC;
            if( seconds >= RUNTIME )
                break;
        }
    }
    return 0;
}
